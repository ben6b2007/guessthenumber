# Simple COBOL progoram -- guess the number (開口中)

## Environment
* OpenCobolIDE 4.7.6

## Rules
1. All players are divided into two teams
2. First, the system will generate a number (range: 1-100)
3. Each team should guess a number by turns
4. If the number is not equal to the generated number, the guessable range will be limited. 
5. Game over: the team who guessed correctly will lose the game.

----------------

## Example
1. First, input two teams name
    <img src="1.png">

2. Then, input the number that you guess, if the guessed number is not equal to the generated number, the guessable range will be limited.
    <img src = '2.png'>

3. If guessed, the system will show the game over message and the generated number, and the team name who won the game. 
    <img src = '3.png'>
           IDENTIFICATION DIVISION.
               PROGRAM-ID. HELLO.
           DATA DIVISION.
           WORKING-STORAGE SECTION.
               01 CURR-USER PIC A(30).
               01 USER-TABLE.
                   05 RECORD-USER OCCURS 3 TIMES.
                       10 USER-NAME PIC A(30).
               01 MIN-NUMBER PIC 99 VALUE 1.
               01 MAX-NUMBER PIC 99 VALUE 99.
               01 PLAYER PIC 9(1) VALUE 1.
               01 RANDOM-NUMBER PIC 9(2).
               01 WS-INPUT_NUMBER PIC 9(2) VALUE 1.
               01 WS-GUESS_NUMBER PIC 9(2).
               01  TEMP-TIME-1.
                   05  T1-HOURS                 PIC 99.
                   05  T1-MINUTES               PIC 99.
                   05  T1-SECONDS               PIC 99.
                   05  T1-HUNDREDTHS-OF-SECS    PIC 99.
           PROCEDURE DIVISION.
           DISPLAY 'Guess the number!!!'.

      *>      Enter the A team name
           DISPLAY 'Enter Red Team Name: '
           PERFORM
                   ACCEPT CURR-USER FROM SYSIN
           END-PERFORM.
           MOVE CURR-USER TO USER-TABLE.
      *>      Enter the A team name
           DISPLAY 'Enter Blue Team Name: '
           PERFORM
                   ACCEPT CURR-USER FROM SYSIN
           END-PERFORM.
      *>          Add user to user table
           MOVE FUNCTION
               CONCATENATE(USER-NAME(1), CURR-USER) TO USER-TABLE.

      *>      Generate random number
           RAN-PARA.
               ACCEPT TEMP-TIME-1 FROM TIME
               COMPUTE RANDOM-NUMBER =
                   FUNCTION RANDOM(T1-HUNDREDTHS-OF-SECS*T1-SECONDS)
                       *100000000000 + 1.
               IF RANDOM-NUMBER IS EQUAL TO 00
                   GO TO RAN-PARA.
               PERFORM ASK-PARA.

      *>      Input number
               ASK-PARA.
               DISPLAY 'Please key in a number From '
                   MIN-NUMBER ' to ' MAX-NUMBER
               PERFORM
                   ACCEPT WS-INPUT_NUMBER FROM SYSIN
               END-PERFORM
               IF WS-INPUT_NUMBER < 0
                   GO TO ASK-PARA.
               IF WS-INPUT_NUMBER > 0
                   MOVE WS-INPUT_NUMBER TO WS-GUESS_NUMBER.
                   GO TO CHK_ZERO-PARA.

      *>      Check number is not equal to zero
               CHK_ZERO-PARA.
               if WS-GUESS_NUMBER IS EQUAL TO 0
                   DISPLAY 'Please input correct number'
                   Go To ASK-PARA.
               if WS-GUESS_NUMBER > 0
                   Go TO CHK_INPUT_SIZE-PARA.

      *>      Check input number is within the range
               CHK_INPUT_SIZE-PARA.
               IF WS-GUESS_NUMBER IS LESS MAX-NUMBER AND
                   WS-GUESS_NUMBER IS GREATER MIN-NUMBER
                   PERFORM CHK_ANS-PARA.
               IF WS-GUESS_NUMBER IS GREATER MAX-NUMBER OR
                   WS-GUESS_NUMBER IS LESS MIN-NUMBER
                   DISPLAY 'Guess number should within the range'.
                   PERFORM ASK-PARA.

      *>      Check is guessed or not
               CHK_ANS-PARA.
                   COMPUTE PLAYER =
                       FUNCTION MOD(PLAYER,2) + 1.
                   IF WS-GUESS_NUMBER IS EQUAL TO RANDOM-NUMBER
                       GO TO END_GAME-PARA.
                  IF WS-GUESS_NUMBER IS NOT EQUAL TO RANDOM-NUMBER
                       GO TO CHK_BIG_SMALL-PARA.

      *>      Handling event of not guessed
               CHK_BIG_SMALL-PARA.
                   IF WS-GUESS_NUMBER IS GREATER THAN RANDOM-NUMBER
                       GO TO BIGGER-PARA.
                   IF WS-GUESS_NUMBER IS LESS THAN RANDOM-NUMBER
                       GO TO SMALLER-PARA.

               BIGGER-PARA.
                   MOVE WS-GUESS_NUMBER TO MAX-NUMBER.
                   DISPLAY MIN-NUMBER ' TO ' WS-GUESS_NUMBER.
                   GO TO ASK-PARA.

               SMALLER-PARA.
                   MOVE WS-GUESS_NUMBER TO  MIN-NUMBER.
                   DISPLAY WS-GUESS_NUMBER ' TO ' MAX-NUMBER.
                   GO TO ASK-PARA.

      *>      Handling event of guessed
               END_GAME-PARA.
                   DISPLAY 'GAME OVER!!!!'
                   DISPLAY FUNCTION
                       TRIM(USER-NAME(PLAYER)) ' Win!!!!!'.
                   DISPLAY "My GUESS NUMBER is : "WS-GUESS_NUMBER.
                   DISPLAY "The Answer is : "RANDOM-NUMBER.
                   STOP RUN.
